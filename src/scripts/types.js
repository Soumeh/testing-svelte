export type Array2 = [number, number]
export type Array3 = [number, number, number]
export type Array4 = [number, number, number, number]

export interface Character extends ModelPart {

    maxLength: number

}

export interface ModelData {

    characterDefaults: {[key: string]: object}
    textureSize: [number, number]
    textHeight: number
    textures: {[key: string]: string}

    characters: {[key: string]: Character}

}

export interface ModelPart {

    texture: string
    texture_size: Array2
    position: Array3
    rotation: Array3

    name: string
    shapes: Array<ModelShape>
    parts: Array<ModelPart>

}

export interface ModelShape {

    type: 'block' | 'plane'
    from: Array3
    to: Array3

    position: Array3

    uv: Array2
    invert?: boolean
    texture_mirror?: boolean

}

