import * as THREE from 'three'
import { scene } from './scene'

const faceIndexes: {[key: string]: number} = {
    west: 0,
    east: 2,

    up: 4,
    down: 6,

    north: 8,
    south: 10,
}

type Array2 = [number, number]
type Array3 = [number, number, number]
type Array4 = [number, number, number, number]

interface ModelData {

    texture: string
    texture_size: Array2
    position: Array3
    rotation: Array3

    shapes?: [ModelCube]
    parts?: [ModelData]

}

interface ModelCube {

    type: 'block' | 'plane'
    from: Array3
    to: Array3

    position: Array3

    uv: Array2
    invert?: boolean
    texture_mirror?: boolean

}

export function parseModel(
    modelData: ModelData,
    texture: string = '',
    textureSize: Array2 = [0, 0],
    position: Array3 = [0, 0, 0],
    rotation: Array3 = [0, 0, 0]
){

    if (modelData.texture) var texture = modelData.texture
    if (modelData.texture_size) var textureSize = modelData.texture_size
    if (modelData.position) var position = combineLists(position, modelData.position) as Array3
    if (modelData.rotation) var rotation = combineLists(rotation, modelData.rotation) as Array3

    if (modelData.shapes) {

        var textureImage = new Image()
        textureImage.src = texture
        var modelTexture = new THREE.Texture(textureImage)

        modelTexture.magFilter = THREE.NearestFilter
        modelTexture.minFilter = THREE.NearestFilter

        textureImage.onload = function() { modelTexture.needsUpdate = true }

        for (const shape of modelData.shapes) {
            // W.I.P. maybe don't do this, maybe...
            if (shape.type != 'block') continue
            const mesh = genMesh(shape, modelTexture, textureSize, position, rotation)
            scene.add(mesh)
        }

    }

    for (const part of modelData.parts || []) {
        parseModel(part, texture, textureSize, position, rotation)
    }

}

function genMesh(cube: ModelCube, texture: THREE.Texture, textureSize: Array2, position: Array3, rotation: Array3) {

    cube.from = combineLists(cube.from, position) as Array3
    cube.to = combineLists(cube.to, position) as Array3

    const size: Array3 = [
        Math.abs(cube.to[0]-cube.from[0]),
        Math.abs(cube.to[1]-cube.from[1]),
        Math.abs(cube.to[2]-cube.from[2])
    ]

    // create cube

    var geo = new THREE.BoxGeometry( ...size )

    // rotate cube

    if (rotation) {
        geo.rotateX(Math.PI / 180 * rotation[0])
        geo.rotateY(Math.PI / 180 * rotation[1])
        geo.rotateZ(Math.PI / 180 * rotation[2])
    }

    // create material

    var mat = new THREE.MeshLambertMaterial({
        color: 0xffffff,
        map: texture,
        side: 2,
        alphaTest: 0.05
    })
    
    // inverted cubes
    
    if (cube.invert) mat.side = THREE.BackSide
    else mat.side = THREE.FrontSide

    // create mesh

    var mesh = new THREE.Mesh( geo, mat )

    const newPos = combineLists(cube.from, position) as Array3
    geo.translate(-cube.from[0], -cube.from[1], -cube.from[2])
    if (cube.position) geo.translate( ...cube.position )
    mesh.position.set( ...newPos )

    // texture mapping

    var uvs = mapCubeUV(size, cube.uv, cube.texture_mirror)

    for (var face in uvs) {
        var fIndex = faceIndexes[face]

        let uvArray = getUVArray(uvs[face], textureSize)
        var geoArray = mesh.geometry.attributes.uv.array
        geoArray.set(uvArray[0], fIndex*4 + 2)  //1,1
        geoArray.set(uvArray[1], fIndex*4 + 0)  //0,1
        geoArray.set(uvArray[2], fIndex*4 + 6)  //1,0
        geoArray.set(uvArray[3], fIndex*4 + 4)  //0,0
        mesh.geometry.uvsNeedUpdate = true
    }

    return mesh
}

function combineLists(first: Array<number>, second: Array<number>) {
    return [  first[0]+second[0], first[1]+second[1], first[2]+second[2] ]
}

function mapCubeUV(cube_size: Array3, uv: Array2, mirror: boolean = false): {[key: string]: Array4} {

    const x = cube_size[0]
    const y = cube_size[1]
    const z = cube_size[2]

    if (mirror) return {
        south: [uv[0],     uv[1],   uv[0]+x,     uv[1]+y],
        north: [uv[0]+z+x, uv[1],   uv[0]+x+z+x, uv[1]+y],

        west:  [uv[0]-z,   uv[1],   uv[0],       uv[1]+y],
        east:  [uv[0]+x,   uv[1],   uv[0]+x+z,   uv[1]+y],

        up:    [uv[0]+x,   uv[1]-z, uv[0],     uv[1]  ],
        down:  [uv[0]+x+x, uv[1],   uv[0]+x,   uv[1]-z]
    }

    return {
        north: [uv[0],     uv[1],   uv[0]+x,     uv[1]+y],
        south: [uv[0]+z+x, uv[1],   uv[0]+x+z+x, uv[1]+y],

        east:  [uv[0]-z,   uv[1],   uv[0],       uv[1]+y],
        west:  [uv[0]+x,   uv[1],   uv[0]+x+z,   uv[1]+y],

        up:    [uv[0],     uv[1]-z,   uv[0]+x,     uv[1]],
        down:  [uv[0]+x,   uv[1],   uv[0]+x+x,   uv[1]-z]
    }
}

function getUVArray(face: [number, number, number, number], textureSize: Array2): Array<[number, number]> {
    return [
        [face[2]/textureSize[0], 1-(face[1]/textureSize[1])],
        [face[0]/textureSize[0], 1-(face[1]/textureSize[1])],
        [face[2]/textureSize[0], 1-(face[3]/textureSize[1])],
        [face[0]/textureSize[0], 1-(face[3]/textureSize[1])]
    ]
}